<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TagType;use App\Models\Project;
use App\Models\Team;use App\Models\Client;
class PublicConttroller extends Controller
{
    public function works(Request $request){
        if($request->id){
            $project        = Project::find($request->id);
            $getProjects    = Project::orderBy('id', 'DESC')->limit(4)->get();
            return view('frontend.work-details',compact('project','getProjects'));
        }else{
            $getProjects    = Project::get();
        }
        
        $getClients     = Client::get()->chunk(8);
        return view('frontend.works',compact('getProjects','getClients'));
    }
}
