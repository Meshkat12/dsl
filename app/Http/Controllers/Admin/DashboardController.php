<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\Client;
use App\Models\Project;
use App\Models\Team;
class DashboardController extends Controller
{
    public function index(){
        $title          = "Dashboard";

        $getServicies   = 0;
        $getProjects    = 0;
        $getTeam        = 0;
        $getClient      = 0;

        return View('admin.index',compact('title','getServicies','getProjects','getTeam','getClient'));
    }
}
