<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
class AccountController extends Controller
{
    private $fileDir    = 'admin.account.';
    private $title      = 'Account';
    private $route      = 'account::';
    private $model      = "App\Models\Account";

    public function index(){
        $title          = $this->title." List";
        $getDatas       = $this->model::get();
        return view($this->fileDir.'index',[
            'title'     => $title,
            'getDatas'  => $getDatas,
            'route'     => $this->route
        ]);
    }
    public function getForm(Request $request){
        // return 1;
        $data           = $this->model::find($request->id);
        $title          = $data ? 'Update '.$data->name : 'Create '.$this->title;
        return view($this->fileDir.'form',[
            'title'     => $title,
            'data'      => $data,
            'route'     => $this->route
        ]);
    }
    public function save(Request $request){
        
        ini_set('memory_limit','-1');
        $validator = Validator::make($request->all(), []);
        $data         = [];
        
        $data         = [
            'name'        => strip_tags($request->name),
            'balance'     => $request->balance,
        ];
        
        try {
            if($request->id){
                $submit     = $this->model::find($request->id);
                $update     = $submit->update($data);
                notify()->success('Data Successfully Updated !');
            }else{
                $submit     = $this->model::create($data);
                notify()->success('Data Successfully Created  !');
            }
            return redirect()->back();
        }catch (\Illuminate\Database\QueryException $ex) {
            notify()->error('problem To Submit Data');
            return redirect()->back()->withErrors($ex->getMessage())
                ->with('myexcep', $ex->getMessage())->withInput();
        }
        return $data;
    }
    public function delete(Request $request){
        $data   = $this->model::find($request->id);
        $del    = $data->delete();
        notify()->success('Data Successfully Deleted  !');
        return redirect()->back();
    }
}
