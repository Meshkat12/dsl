<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
class TransectionController extends Controller
{
    private $fileDir        = 'admin.transection.';
    private $title          = 'Transection';
    private $route          = 'account::transection.';
    private $model          = "App\Models\Transaction";
    private $modelAccount   = "App\Models\Account";
    private $modelType      = "App\Models\TransactionType";

    public function index(){
        $title          = $this->title." List";
        $getDatas       = $this->model::get();
        $getTypeDatats  = $this->modelType::get();
        return view($this->fileDir.'index',[
            'title'         => $title,
            'getDatas'      => $getDatas,
            'getTypeDatats' => $getTypeDatats,
            'route'         => $this->route
        ]);
    }
    public function getForm(Request $request){
        // return 1;
        $data           = $this->model::find($request->id);
        $accountData    = $this->modelAccount::get();
        $typeData       = $this->modelType::get();
        $title          = $data ? 'Update Transection' : 'Create '.$this->title;
        return view($this->fileDir.'form',[
            'title'         => $title,
            'data'          => $data,
            'accountData'   => $accountData,
            'typeData'      => $typeData,
            'route'         => $this->route
        ]);
    }
    public function save(Request $request){
        
        ini_set('memory_limit','-1');
        $validator = Validator::make($request->all(), [
            'amount'    => 'required|numeric|gt:0',
        ]);
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            notify()->error($error);
            return redirect()->back();
        }
        $data         = [];
        
        $data         = [
            'account_id'    => strip_tags($request->account_id),
            'amount'        => $request->amount,
            'type'          => strip_tags($request->type),
            'date'          => $request->date ? $request->date : date('Y-m-d'),
        ];
        $getAccount = $this->modelAccount::find($request->account_id);
        $getType    = $this->modelType::find($request->type);
        if(!$getAccount){
            notify()->error('Account data not foud! ');
            return redirect()->back();
        }
        try {
            if($request->id){
                $submit           = $this->model::find($request->id);
                if($getType->credit_debit == 'c'){
                    $dataAccount      = [
                        'balance'     => ($getAccount->balance - $submit->amount) + ( $request->amount ),
                    ];
                }else{
                    $dataAccount      = [
                        'balance'     => ($getAccount->balance - $submit->amount) - ( $request->amount ),
                    ];
                }
                
                $update           = $submit->update($data);
                $updateAccount    = $getAccount->update($dataAccount);

                notify()->success('Data Successfully Updated !');
            }else{
                $submit           = $this->model::create($data);
                if($getType->credit_debit == 'c'){
                    $dataAccount      = [
                        'balance'     => $getAccount->balance + $request->amount,
                    ];
                }else{
                    $dataAccount      = [
                        'balance'     => $getAccount->balance - $request->amount,
                    ];
                }
                
                $updateAccount    = $getAccount->update($dataAccount);
                notify()->success('Data Successfully Created  !');
            }
            return redirect()->back();
        }catch (\Illuminate\Database\QueryException $ex) {
            notify()->error('problem To Submit Data');
            return $ex->getMessage();
            return redirect()->back()->withErrors($ex->getMessage())
                ->with('myexcep', $ex->getMessage())->withInput();
        }
        return $data;
    }
    public function delete(Request $request){
        $data   = $this->model::find($request->id);
        $getAccount = $this->modelAccount::find($data->account_id);
        $getType    = $this->modelType::find($data->type);
        if($getType->credit_debit == 'c'){
            $dataAccount      = [
                'balance'     => ($getAccount->balance - $data->amount),
            ];
        }else{
            $dataAccount      = [
                'balance'     => ($getAccount->balance + $data->amount),
            ];
        }
        
        $updateAccount    = $getAccount->update($dataAccount);
        $del              = $data->delete();
        
        notify()->success('Data Successfully Deleted  !');
        return redirect()->back();
    }
}
