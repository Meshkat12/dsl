<?php

use Illuminate\Support\Facades\Route;
$adminNamespace = 'App\Http\Controllers\Admin';
$Namespace = 'App\Http\Controllers';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::namespace($adminNamespace)->middleware(['auth'])->group(function () {
    Route::get('/home', 'DashboardController@index')->name('index');
    Route::get('/admin', 'DashboardController@index')->name('admin');
    Route::get('/', 'DashboardController@index')->name('home');
    Route::get('/logout', 'Auth\AuthController@logout')->name('Auth::logout');
    
    Route::prefix('account')->name('account::')->group(function () {
        Route::get('', 'AccountController@index')->name('list');
        Route::post('save', 'AccountController@save')->name('save');
        Route::get('/form', 'AccountController@getForm')->name('form');
        Route::get('/delete', 'AccountController@delete')->name('delete');

        Route::prefix('transection')->name('transection.')->group(function () {
            Route::get('', 'TransectionController@index')->name('list');
            Route::post('save', 'TransectionController@save')->name('save');
            Route::get('/form', 'TransectionController@getForm')->name('form');
            Route::get('/delete', 'TransectionController@delete')->name('delete');

            Route::prefix('type')->name('type.')->group(function () {
                Route::get('', 'TransectionTypeController@index')->name('list');
                Route::post('save', 'TransectionTypeController@save')->name('save');
                Route::get('/form', 'TransectionTypeController@getForm')->name('form');
                Route::get('/delete', 'TransectionTypeController@delete')->name('delete');
            });
        });
    });
});

