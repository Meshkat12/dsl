<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">{{$title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="">
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                        <form action="{{route($route.'save')}}" method="post" novalidate="novalidate" enctype='multipart/form-data'>
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label mb-1">Name</label>
                                        <input id="name" name="name" type="text" class="form-control" value="{{$data ? $data->name : null}}" placeholder="Name">
                                        @if($data)
                                        <input id="id" name="id" type="hidden" class="form-control" value="{{$data ? $data->id : null}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="balance" class="control-label mb-1">Balance</label>
                                        <input id="balance" name="balance" type="number" min="0" class="form-control" value="{{$data ? $data->balance : null}}" {{$data && $data->balance > 0 ? 'readonly' : null}} placeholder="Banance">
                                    </div>
                                </div>
                            </div>
                            
                            <div>
                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block bg-info">
                                    <i class="fa fa-send fa-lg"></i>&nbsp;
                                    <span id="payment-button-amount">Submit</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>