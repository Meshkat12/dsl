<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">{{$title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div id="pay-invoice">
            <div class="card-body">
                <form action="{{route($route.'save')}}" method="post" novalidate="novalidate" enctype='multipart/form-data'>
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="control-label mb-1">Account</label>
                                <select class="form-control" name="account_id">
                                    <option value=""> Select Account</option>
                                    @foreach($accountData as $account)
                                    <option value="{{ $account->id }}" {{ $data && $account->id == $data->account_id ? 'Selected' : null }}> {{ $account->name }} </option>
                                    @endforeach
                                </select>
                                @if($data)
                                <input id="id" name="id" type="hidden" class="form-control" value="{{$data ? $data->id : null}}">
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="control-label mb-1">Type</label>
                                <select class="form-control" name="type">
                                    <option value=""> Select Type</option>
                                    @foreach($typeData as $type)
                                    <option value="{{ $type->id }}" {{ $data && $type->id == $data->type ? 'Selected' : null }}> {{ $type->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="amount" class="control-label mb-1">Amount</label>
                                <input id="amount" name="amount" type="number" min="0" class="form-control" value="{{$data ? $data->amount : null}}" placeholder="Amouunt">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date" class="control-label mb-1">Date</label>
                                <input id="date" name="date" type="date" min="0" class="form-control" value="{{$data ? $data->date : date('Y-m-d')}}" placeholder="Date">
                            </div>
                        </div>
                    </div>
                    
                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block bg-info">
                            <i class="fa fa-send fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Submit</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>