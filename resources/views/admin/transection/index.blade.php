@extends('layouts.master')
@section('contents')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{$title}}</strong>
                
                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#create" onclick="getForm(0)">
                    <i class="fa fa-plus"></i> Create Ttransaction
                </button>
                <button type="button" class="btn btn-warning float-right mx-3" data-toggle="modal" data-target="#create" onclick="getTypeForm(0)">
                    <i class="fa fa-plus"></i> Create Type
                </button>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-5">
                        <table class="table table-bordered">
                            <thead class="bg-warning">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Type Name</th>
                                    <th scope="col">Credit / Debit</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($getTypeDatats as $type)
                                <tr>
                                    <th scope="row">{{ $loop->index+1}}</th>
                                    <td>{{$type->name }}</td>
                                    <td>{{$type->credit_debit == 'c' ? 'Credit' : 'Debit'}}</td>
                                    <td>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#create" onclick="getTypeForm({{$type->id}})" > 
                                            <i class="fa fa-edit"></i> 
                                        </button>
                                        <a href="{{ route($route.'type.delete',['id'=>$type->id]) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"> 
                                            <i class="fa fa-trash"></i> 
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead class="bg-primary color-white">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Account Name</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Type</th>
                            <th scope="col">Credit / Debit</th>
                            <th scope="col">Date</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($getDatas as $data)
                        <tr>
                            <th scope="row">{{ $loop->index+1}}</th>
                            <td>{{$data->account ? $data->account->name : null}}</td>
                            <td>{{$data->amount}}</td>
                            <td>{{$data->getType ? $data->getType->name : null}}</td>
                            <td>{{$data->getType && $data->getType->credit_debit == 'c' ? 'Credit' : 'Debit'}}</td>
                            <td>{{$data->date}}</td>
                            <td>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#create" onclick="getForm({{$data->id}})" > 
                                    <i class="fa fa-edit"></i> 
                                </button>
                                <a href="{{ route($route.'delete',['id'=>$data->id]) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"> 
                                    <i class="fa fa-trash"></i> 
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" id="form">
       
    </div>
</div>
@stop
@section('js')
<script src="https://code.jquery.com/jquery-3.6.3.js"></script>
<script>
    function getForm(id){

        $.ajax({
            url: "{{ route($route.'form') }}",
            method: 'get',
            data:{ id:id },
            success: function(result){
                $('#form').html(result);
            }
        });
    }
    function getTypeForm(id){

        $.ajax({
            url: "{{ route($route.'type.form') }}",
            method: 'get',
            data:{ id:id },
            success: function(result){
                $('#form').html(result);
            }
        });
    }
</script>
@stop
@section('css')
<style>
    svg{
        height : 35px;
    }
</style>
@stop