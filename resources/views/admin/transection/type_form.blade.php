<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">{{$title}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="">
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                        
                        <form action="{{route($route.'type.save')}}" method="post" novalidate="novalidate" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-group">
                                <label for="name" class="control-label mb-1">Type Name</label>
                                <input id="name" name="name" type="text" class="form-control" value="{{$data ? $data->name : null}}">
                                @if($data)
                                <input id="id" name="id" type="hidden" class="form-control" value="{{$data ? $data->id : null}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="name" class="control-label mb-1">Type Name</label>
                                <select class="form-control" name="credit_debit">
                                    <option value="">Select Credit/Debit</option>
                                    <option value="c" {{$data && $data->credit_debit == 'c' ? 'Selected' : null}}>Credit</option>
                                    <option value="d" {{$data && $data->credit_debit == 'd' ? 'Selected' : null}}>Debit</option>
                                </select>
                            </div>
                            <div>
                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block bg-info">
                                    <i class="fa fa-send fa-lg"></i>&nbsp;
                                    <span id="payment-button-amount">Submit</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>