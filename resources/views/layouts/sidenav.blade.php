<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('home')}}"><i class="menu-icon fa  fa-dashboard"></i>Dashboard </a>
                </li>
                <li class="menu-title">Modules</li>
                
                <li class="menu-item-has-children dropdown 
                    {{ Request::routeIs('account::list') ? 'show' : '' }} ||
                    {{ Request::routeIs('account::transection.list') ? 'show' : '' }} ||
                    {{ Request::routeIs('account::transection.type.list') ? 'show' : '' }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                        <i class="menu-icon fa fa-sitemap"></i>Account
                    </a>
                    <ul class="sub-menu children dropdown-menu 
                        {{ Request::routeIs('account::list') ? 'show' : '' }} ||
                        {{ Request::routeIs('account::transection.list') ? 'show' : '' }} ||
                        {{ Request::routeIs('account::transection.type.list') ? 'show' : '' }}">                            
                        <li class="{{ Request::routeIs('account::list') ? 'active' : '' }}">
                            <i class="fa fa-puzzle-piece"></i>
                            <a href="{{route('account::list')}}">Accounts</a>
                        </li>
                        <li class="{{ Request::routeIs('account::transection.list') ? 'active' : '' }}"><i class="fa fa-id-badge"></i><a href="{{route('account::transection.list')}}">Transection</a></li>
                    </ul>
                </li>
                
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>