1. Clone the project
2. update/install composer
3. add .env to the root in project
4. copy .env.example to .env
5. add database to .env
6. type " php artisasn migrate " in cmd/tetrminal
7. create a user in user table 
8. type " php artisan db:seed --class=AccountSeeder "
9. type " php artisan serve "

PHP -v 8+